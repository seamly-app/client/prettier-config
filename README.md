## Seamly prettier-config

This is the default prettier configuration for `@seamly/*` implementations

## Installation

```
yarn add -D @seamly/prettier-config
// or
npm install -D @seamly/prettier-config
```

## Usage

This package can be used as `@seamly/prettier-config` following the Prettier documentation on [Sharing configuration](https://prettier.io/docs/en/configuration.html#sharing-configurations)

#### Editing `package.json`

```jsonc
{
  // ...
  "prettier": "@seamly/prettier-config"
}
```
