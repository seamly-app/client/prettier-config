# Seamly Prettier Config Changelog

## [Unreleased]

## 3.2.0 (5 September 2024)

### Changed

- Updated `prettier` to 3.3.x

## 3.1.0 (2 February 2024)

### Changed

- Updated `prettier` to 3.2.x

## 3.0.3 (5 October 2023)

### Changed

- Updated `prettier` to 3.0.x

## 3.0.0 (7 August 2023)

### Changed

- Updated `prettier` to 3.0.x

## 2.2.0 (7 November 2022)

### Changed

- Updated `prettier` to 2.7.x

## 2.1.0 (10 June 2022)

### Changed

- Updated `prettier` to 2.6.x

## 2.0.0 (27 October 2021)

### Changed

- Lock `prettier` devDependency and peerDependency to 2.4.x

## 1.0.2 (25 January 2021)

### Changed

- Lock `prettier` devDependency and peerDependency to 1.18.x

## 1.0.1 (22 January 2021)

### Changed

- Downgraded `prettier` devDependency and peerDependency from 2.x to 1.x
